# Meteor Kickstart

This is a Meteor project stub. Feel free to fork it in order to kickstart a new project. 

Main features:
- **React-Bootstrap** for building UIs quickly. See https://react-bootstrap.github.io
- **Flow Router**
- **Unit tests** with Mocha (web reporter and PhantomJS), Sinon and Chai

Gitlab continuous integration support:
- runs unit tests after each push
- builds a Docker image on demand (use Git tag "docker")

Also:
- aldeed:simple-schema
- aldeed:collection2
